var syntax = 'sass'; // Syntax: sass or scss;

var gulp = require('gulp'),
    gutil = require('gulp-util'),
    del = require('del'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    cleancss = require('gulp-clean-css'),
    cache = require('gulp-cache'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    notify = require("gulp-notify"),
    rsync = require('gulp-rsync');


gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: 'src'
        },
        notify: false,
        // open: false,
        // online: false, // Work Offline Without Internet Connection
        // tunnel: true, tunnel: "projectname", // Demonstration page: http://projectname.localtunnel.me
    })
});

gulp.task('styles', function () {
    return gulp.src('src/' + syntax + '/**/*.' + syntax + '')
        .pipe(sass({outputStyle: 'expanded'}).on("error", notify.onError()))
        .pipe(rename({suffix: '.min', prefix: ''}))
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(cleancss({level: {1: {specialComments: 0}}})) // Opt., comment out when debugging
        .pipe(gulp.dest('src/css'))
        .pipe(browserSync.stream())
});

gulp.task('js', function () {
    return gulp.src([
        'src/libs/jquery/dist/jquery.min.js',
        'src/libs/popper.js/dist/popper.js',
        'src/libs/bootstrap4/dist/js/bootstrap.min.js',
        'src/js/common.js' // Always at the end
    ])
        .pipe(concat('scripts.min.js'))
        // .pipe(uglify()) // Mifify js (opt.)
        .pipe(gulp.dest('src/js'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('copy-js-libs', function () {
    return gulp.src([
        // 'src/libs/popper.js/dist/popper.min.js'
    ])
        .pipe(gulp.dest('src/js'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('copy-css-libs', function () {
    return gulp.src([
        // 'src/libs/owl.carousel/dist/assets/owl.theme.default.min.css'
    ])
        .pipe(gulp.dest('src/css'))
        .pipe(browserSync.stream())

});

gulp.task('rsync', function () {
    return gulp.src('src/**')
        .pipe(rsync({
            root: 'src/',
            hostname: 'username@yousite.com',
            destination: 'yousite/public_html/',
            // include: ['*.htaccess'], // Includes files to deploy
            exclude: ['**/Thumbs.db', '**/*.DS_Store'], // Excludes files from deploy
            recursive: true,
            archive: true,
            silent: false,
            compress: true
        }))
});

gulp.task('watch', ['copy-js-libs', 'copy-css-libs', 'styles', 'js', 'browser-sync'], function () {
    gulp.watch('src/' + syntax + '/**/*.' + syntax + '', ['styles']);
    gulp.watch(['libs/**/*.js', 'src/js/common.js'], ['js']);
    gulp.watch('src/*.html', browserSync.reload)
});

gulp.task('imagemin', function () {
    return gulp.src('src/img/**/*')
        .pipe(cache(imagemin())) // Cache Images
        .pipe(gulp.dest('dist/img'));
});

gulp.task('build', ['removedist', 'imagemin', 'copy-js-libs', 'copy-css-libs', 'styles', 'js'], function () {

    var buildFiles = gulp.src([
        'src/*.html',
        'src/ht.access'
    ]).pipe(gulp.dest('dist'));

    var buildCss = gulp.src([
        'src/css/*.min.css'
    ]).pipe(gulp.dest('dist/css'));

    var buildJs = gulp.src([
        'src/js/*.min.js'
    ]).pipe(gulp.dest('dist/js'));

    var buildFonts = gulp.src([
        'src/fonts/**/*',
    ]).pipe(gulp.dest('dist/fonts'));

});

gulp.task('removedist', function () {
    return del.sync('dist');
});

gulp.task('default', ['watch']);
